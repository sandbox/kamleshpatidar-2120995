<?php
/**
 * @file media_metacafe/includes/media-metacafe-video.tpl.php
 *
 * Template file for theme('media_metacafe_video').
 *
 * Variables available:
 *  $uri - The uri to the Dailymotion video, such as metacafe://metacafe_id/118956745/metacafe_title/kamlesh_patidar_test.
 *  $width - The width to render.
 *  $height - The height to render.
 *  $autoplay - If TRUE, then start the player automatically when displaying.
 *  $fullscreen - Whether to allow fullscreen playback.
 *  $output - The object/embed code
 */
?>
<div class="media-metacafe-outer-wrapper" style="width: <?php print $width; ?>px; height: <?php print $height; ?>px;">
  <div class="media-metacafe-preview-wrapper" id="<?php print $wrapper_id; ?>">
    <?php print $output; ?>
  </div>
</div>
