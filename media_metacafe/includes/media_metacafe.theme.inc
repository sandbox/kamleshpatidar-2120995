<?php
/**
 * @file media_metacafe/includes/media_metacafe.theme.inc
 *
 * Theme and preprocess functions for Media: Metacafe.
 */

/**
 * Preprocess function for theme('media_metacafe_video').
 */
function media_metacafe_preprocess_media_metacafe_video(&$variables) {
  // Build the URL for display.
  $uri = $variables['uri'];
  $wrapper = file_stream_wrapper_get_instance_by_uri($uri);
  $parameters = $wrapper->get_parameters();
  $variables['metacafe_id'] = check_plain($parameters['metacafe_id']);
  $variables['metacafe_title'] = check_plain($parameters['metacafe_title']);

  $variables['width'] = isset($variables['width']) ? $variables['width'] : media_metacafe_variable_get('width');
  $variables['height'] = isset($variables['height']) ? $variables['height'] : media_metacafe_variable_get('height');
  $variables['autoplay'] = isset($variables['autoplay']) ? $variables['autoplay'] : media_metacafe_variable_get('autoplay');
  $variables['fullscreen'] = isset($variables['fullscreen']) ? $variables['fullscreen'] : media_metacafe_variable_get('fullscreen');
  $variables['iframe'] = isset($variables['iframe']) ? $variables['iframe'] : media_metacafe_variable_get('iframe');
  $variables['autoplay'] = $variables['autoplay'] ? 1 : 0;
  $variables['fullscreen'] = $variables['fullscreen'] ? 'true' : 'false';

  $variables['wrapper_id'] = 'media_metacafe_' . $variables['metacafe_id'] . '_' . $variables['id'];

  if (!$variables['iframe']) {
    $variables['output'] = <<<OUTPUT
    <object width="{$variables['width']}" height="{$variables['height']}">
      <param name="movie" value="http://www.metacafe.com/fplayer/{$variables['metacafe_id']}/{$variables['metacafe_title']}?autoPlay={$variables['autoplay']}"></param>
      <param name="allowFullScreen" value="{$variables['fullscreen']}"></param>
      <param name="allowScriptAccess" value="always"></param>
      <embed type="application/x-shockwave-flash" src="http://www.metacafe.com/fplayer/{$variables['metacafe_id']}/{$variables['metacafe_title']}?autoPlay={$variables['autoplay']}" width="{$variables['width']}" height="{$variables['height']}" allowfullscreen="{$variables['fullscreen']}" allowscriptaccess="always" wmode="transparent"></embed>
    </object>
OUTPUT;
  }
  else {
    $variables['output'] = <<<OUTPUT
    <iframe frameborder="0" width="{$variables['width']}" height="{$variables['height']}" src="http://www.metacafe.com/embed/{$variables['metacafe_id']}?autoPlay={$variables['autoplay']}"></iframe>
OUTPUT;
  }
}
