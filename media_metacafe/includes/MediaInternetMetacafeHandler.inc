<?php
/**
 * @file media_metacafe/includes/MediaInternetMetacafeHandler.inc
 * media_internet handler for MetaCafe.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetMetacafeHandler extends MediaInternetBaseHandler {
  public function parse($embedCode) {
    $patterns = array(
      '@metacafe\.com/watch/([^/]+)@i',
      '@metacafe\.com/embed/([^/]+)@i',
    );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      if (isset($matches[1])) {
        $metavideotitle = $this->getMetacafeVideoTitle($matches[1]);       
        return file_stream_wrapper_uri_normalize('metacafe://metacafe_id/' . $matches[1] . '/metacafe_title/' . $metavideotitle);
      }
    }
    return FALSE;
  }
  
  /**
   * Returns a file object which can be used for validation 
   */
  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    return file_uri_to_object($uri);
  }
  
  /**
   * Recognize if this handler should take the the item with the embed coded passed as argument. 
   */
  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }
  
  /**
   * To get Metacafe video title from Metacafe Video ID
   * and to validate Metacafe Video ID
   * Metacafe Video title must present for thumbnail creation
   * Embed Metacafe Video does not provide title e.g.
   * http://www.metacafe.com/embed/45787899/
   */
  public function getMetacafeVideoTitle($id) {
    $titleurl = 'http://www.metacafe.com/api/item/' . $id;
    $response = simplexml_load_file($titleurl);
    $videolink = $response->channel->item->link;
    if (isset($videolink)) {
      $videotitle = trim(substr($videolink, strrpos($videolink , '/', -2)), '/');
      return $videotitle;
    }
    else {
      throw new MediaInternetValidationException("The Metacafe video ID is invalid or video is no longer Exist."); 
    }
  }

}
