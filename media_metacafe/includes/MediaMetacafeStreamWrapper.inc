<?php
/**
 * @file media_metacafe/includes/MediaMetacafeStreamWrapper.inc
 * Create a Metacafe Stream Wrapper class for the Media/Resource module.
 */

/**
 * Stream Wrapper class for Metacafe.
 */
class MediaMetacafeStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://www.metacafe.com/watch';

  protected function getTarget($uri = NULL) {
    // It's a read-only stream wrapper for now.
    return FALSE;
  }

  function interpolateUrl() {
    $parameters = $this->get_parameters();
    return $this->base_url . '/' . check_plain($parameters['metacafe_id']) . '/' . check_plain($parameters['metacafe_title']);
  }

  public static function getMimeType($uri, $mapping = NULL) {
    return 'video/metacafe';
  }

  public function getOriginalThumbnailPath() {
    $parameters = $this->get_parameters();
    return 'http://s4.mcstatic.com/thumb/' . check_plain($parameters['metacafe_id']) . '/0/4/directors_cut/0/1/' . check_plain($parameters['metacafe_title']) . '.jpg';
  }

  public function getLocalThumbnailPath() {
    $parameters = $this->get_parameters();
    $local_path = 'public://media-metacafe/' . check_plain($parameters['metacafe_id']) . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($this->getOriginalThumbnailPath(), $local_path);
    }
    return $local_path;
  }
}
