# Media: Metacafe

Media: MetaCafe integrates with the Media module to insert Metacafe videos 
with file fields or directly into WYSIWYG text areas by paste Metacafe Video
url.

- Install Media: Metacafe module at sites/all/modules 
- Select file field. choose the widget type Media File Selector. 
- In Field Setting choose Video as Allowed remote media types and
  select metacafe as allowed uri scheme 
- On Manage display for the file field content, choose
  Rendered file and a view mode.
- For each view mode Metacafe video formatter options are in Configuration -> 
  Media File types -> Video -> Manage file display. Here you can 
  choose size, autoplay options.
- When using the file field while creating or editing content, paste a Metacafe
  video url into the Web tab.
- In wysiwyg editor, paste iframe or embed source code of Metacafe video, 
  it will display video in editor area.